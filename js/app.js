/*
localDB
    -CLIENTS
        -Broker
        -ui
    -SETTINGS
*/
//Load from local storage
var localDB = {};
var localDB_json = JSON.parse(localStorage.getItem("localDB"));
if (localDB_json != null){
    localDB = localDB_json;
}else{
    localDB = {
    "CLIENTS": [],
    "SETTINGS":[]
  }
}
console.log(localDB);

var client_array_index = 0;



//JSON_UI array of objects
var json_ui;

//Android Webview Object.Values fix....
Object.values = (obj) => Object.keys(obj).map(key => obj[key]);

//Global variables

var MQTTclient;
var gauges = [];
var charts = [];
var colorpickers = [];
var form_ui_node_mode = ""; //edit: edit node; new: new node;
var selected_node_id = "";
var ui_edit_enabled = false;


 //init form jscolors
 var jscolor_modal_options = {
 zIndex: 20000
 }

 $.each($(".form_jscolor"), function(){
     console.log(this);
 var picker = new jscolor(this, jscolor_modal_options);
 picker.value="000000";
 });

//save localDB
function saveDB(){
    localStorage.setItem("localDB", JSON.stringify(localDB)); 
}

//save ui_nodes  data to local storage
function save_ui(){
  localDB.CLIENTS[client_array_index].UI = json_ui;
  saveDB();
    modal("UI data saved successfully.");
};

//generate UUID
function create_UUID(){
    
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-xxxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

//Snackbar-Toast function
function modal(message){
    $("#snackbar").html(message);
    $("#snackbar").addClass("show");
   
    setTimeout(function(){ $("#snackbar").removeClass("show"); }, 3000);
}

$(function () {

    //Load clients cards
    $.each(localDB.CLIENTS, function(){
        var html = ` 
        <div class="col-sm-4"style="padding:5px;" >
            <div class="card client_card" style="padding:0px;">
                <div class="card-header">${this.BROKER_CONNECTION.client_name}
                    <div class="card-header-actions">
                        <a class="card-header-action btn_edit_client btn-setting" data-client-id="${this.id}" data-toggle="modal" data-target="#modal_add_client"  href="#">
                            <i class="icon-settings"></i>
                        </a>
                        <a class="card-header-action btn_delete_client btn-close" data-client-id="${this.id}" href="#">
                            <i class="icon-close"></i>
                        </a>
                    </div>
                </div>
                <div class="collapse show" id="collapseExample">
                    <div class="card-body row" style="padding:15px;">
                        <div class="col-sm-12" >
                            <b>Address: </b>${this.BROKER_CONNECTION.broker_address}</br>
                            <b>Port: </b>${this.BROKER_CONNECTION.broker_port}</br>
                            <b>Path: </b>${this.BROKER_CONNECTION.broker_path}</br>
                            <b>ID: </b>${this.BROKER_CONNECTION.broker_client_id}
                        </div>
                        <div class="col-sm-12" style="padding:5px;">
                            <button class="btn btn_broker_connect btn-block btn-primary" data-client-id="${this.id}" type="button">Connect</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>`;

        $("#client_cards").append(html);
    });

    //add new client card and set height to client cards
    var html = `
    <div class="col-sm-4"style="padding:5px;" >
        <div id="card_add_new_client" class="card btn btn_add_new_client" data-toggle="modal" data-target="#modal_add_client"  style="padding:5px; margin:0px; border: dotted; color: gray">
            <span class="center"><center>add new client</center></span>
        </div>
    </div>`;
    $("#client_cards").append(html);
    if($(".client_card").length > 0){
        $("#card_add_new_client").css("height", $(".client_card:first").height())
    }else{
        $("#card_add_new_client").css("height", "100px");
    }
   

    //Add new client button
    $(document).on("click", ".btn_add_new_client", function(){
        $("#form_add_client").data("action", "add_new");
        console.log($("#form_add_client").data("action"));
    });

    //Edit client button
    $(document).on("click", ".btn_edit_client", function(){
        $("#form_add_client").data("action", "edit_client");
        $("#form_add_client").data("client-id", $(this).data("client-id"));
        var client_to_edit_id = $(this).data("client-id");
        $.each(localDB.CLIENTS, function(index, client){
            if(client.id == client_to_edit_id ){
                $("#form_add_client .form-control").each(function(){
                    switch($(this).attr("type")){
                        case "text":
                            $(this).val(client.BROKER_CONNECTION[$(this).attr("name")]);
                        break;
                        case "number":
                            $(this).val(client.BROKER_CONNECTION[$(this).attr("name")]);
                        break;
                        case "select":
                            $(this).val(client.BROKER_CONNECTION[$(this).attr("name")]);
                        break;
                        case "checkbox":
                            $(this).prop("checked",client.BROKER_CONNECTION[$(this).attr("name")]);   
                        break;
        
                    }
                });
            }
        });  
    });

    //Delete client button
    $(document).on("click", ".btn_delete_client", function(e){
        e.preventDefault();
        var client_to_delete_id = $(this).data("client-id");
        Swal.fire({
          title: '<strong>Client delete</strong>',
          icon: 'warning',
          html:'Are you sure you want to delete the client?',
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText:'<i class="fa fa-trash"></i> yes, delete',
          confirmButtonColor: '#FF0000',
          cancelButtonText:'<i class="fa fa-times"></i>no'
        }).then((result) => {
          if (result.value) {
            $.each(localDB.CLIENTS, function(index, client){
              if(client.id == client_to_delete_id ){
                  var index = localDB.CLIENTS.indexOf(client);
                  if (index > -1) {
                      localDB.CLIENTS.splice(index, 1);
                      console.log(localDB.CLIENTS);
                      saveDB();
                      window.location.reload();
                  }
              }
            });
          }
        });
    });

    //Add new /Edit client form submit
    $("#form_add_client").on("submit",function(event){
        event.preventDefault();
        var formdata = $(this).serializeArray();

        //add form checkboxes
        $("#form_add_client input:checkbox").each(function(){
            var checkbox = {}
            checkbox.name = this.name;
            if(this.checked){
                checkbox.value = true;
            }else{
                checkbox.value = false;
            }
            formdata.push(checkbox);
            
        });

        switch ($(this).data("action")){
            case "add_new":
                //New client object
                var new_client = {};
                new_client.id = create_UUID();
                new_client.BROKER_CONNECTION = {} ;
                new_client.UI = {};
                new_client.UI.nodes = [];

                $.each(formdata, function(){
                   new_client.BROKER_CONNECTION[this.name] = this.value;
                });
                
                localDB.CLIENTS.push(new_client);
            break;
            //Edit client
            case "edit_client":
                var client_id_to_edit = $(this).data("client-id");
                $.each(formdata, function(){
                    var name = this.name;
                    var value = this.value;
                    
                    $.each(localDB.CLIENTS, function(index, client){
                        if(client.id == client_id_to_edit ){
                           client.BROKER_CONNECTION[name]=value;      
                        }
                    });
                });
            break;
        }    
        saveDB();
        window.location.reload();
    });

    //Client Connect button
    $(document).on("click", ".btn_broker_connect", function(e){
        $(".btn_broker_connect").prop("disabled", true);
        //Select the object of the client to be connecting
        var client_to_connect = $(this).data("client-id");
        $.each(localDB.CLIENTS, function(index, client){
            if(client.id == client_to_connect ){
                //if connection has path added
                console.log(client);
                if(client.BROKER_CONNECTION.broker_path != ""){
                    MQTTclient = new Paho.Client(client.BROKER_CONNECTION.broker_address, Number(client.BROKER_CONNECTION.broker_port), client.BROKER_CONNECTION.broker_path,  client.BROKER_CONNECTION.broker_client_id);
                }else{
                    MQTTclient = new Paho.Client(client.BROKER_CONNECTION.broker_address, Number(client.BROKER_CONNECTION.broker_port),  client.BROKER_CONNECTION.broker_client_id);
                }

                
                // set callback handlers
                MQTTclient.onConnectionLost = onConnectionLost;
                MQTTclient.onMessageArrived = onMessageArrived;
            
                //Options
                var options = {
                timeout: 600,
                keepAliveInterval: Number(client.BROKER_CONNECTION.broker_keepalive),
                onSuccess: onConnect,
                onFailure: onFail,
                cleanSession: client.BROKER_CONNECTION.broker_cleansession,
                useSSL: client.BROKER_CONNECTION.broker_useSSL,
                userName: client.BROKER_CONNECTION.broker_username,
                password:  client.BROKER_CONNECTION.broker_password
                };
                       
            
                if(client.BROKER_CONNECTION.broker_lwat_topic != "" && client.BROKER_CONNECTION.broker_lwat_message != ""){
                var willmessage = new Paho.Message(client.BROKER_CONNECTION.broker_lwat_message);
                willmessage.destinationName = client.BROKER_CONNECTION.broker_lwat_topic;
                willmessage.qos = Number(client.BROKER_CONNECTION.broker_lwat_qos);
                willmessage.retained = client.BROKER_CONNECTION.broker_lwat_retain;
                options.willMessage = willmessage;
                }
                $.each(localDB.CLIENTS, function(index, client){
                    if (client.id == client_to_connect){
                      client_array_index = index;
                    }
                });
                console.log(MQTTclient);
                MQTTclient.connect(options);
            }
        });
    });

    $(document).on("click", "#btn_broker_disconnect", function(event){
      event.preventDefault();
      MQTTclient.disconnect();
      modal("Client Disconnected");
    });

    function onFail(context) {
      console.log("ERROR", "Failed to connect. [Error Message: ", context.errorMessage, "]");
    }

    // called when the client loses its connection
    function onConnectionLost(responseObject) {
        if (responseObject.errorCode !== 0) {
          console.log("onConnectionLost:"+responseObject.errorMessage);
          modal("Client disconnected: "+responseObject.errorMessag);
        }
        MQTTclient ={};
        gauges = [];
        charts = [];
        colorpickers = [];
        form_ui_node_mode = ""; //edit: edit node; new: new node;
        selected_node_id = "";
        ui_edit_enabled = false;
  
        $("#client_ui_menu").hide("slow");
        $("#client_ui").hide("slow");
        $("#client_ui").empty();
        $("#client_cards").show("slow");

        $(".btn_broker_connect").prop("disabled", false);
        
    };

    // called when the client connects
    function onConnect() {
        modal("Client connected!");
        
       $("#client_ui_menu").show("slow");
       $("#client_ui").show("slow");
       $("#client_cards").hide("slow");
       $("#span_connected_broker").html(localDB.CLIENTS[client_array_index].BROKER_CONNECTION.client_name);
       
       json_ui = localDB.CLIENTS[client_array_index].UI;
       $.each(json_ui.nodes, function(){
  
        var ui_node = this;
        
        ui_add_element(ui_node);
        $.each($(".ui_div"), function(){
          interact(this).draggable(false);
        });
        $(".ui_div_helper").css("display", "none");
        
      });

    };
    
    // called when a message arrives
    function onMessageArrived(message) {
        console.log("onMessageArrived:"+message.destinationName);
        //add topic path to array
          var message_nodes = json_ui.nodes.filter(function(ui_node) {
          return ui_node.path == message.destinationName;
        });
        console.log(message_nodes);
        $.each(message_nodes, function(){
          console.log(message.payloadString);
          var ui_node = this;
          switch(ui_node.type){
            
            //Switch
            case "switch":
              switch(message.payloadString){
                case ui_node.message_on:
                  console.log( $("#switch_"+ui_node.id));
                  $("#switch_"+ui_node.id).prop("checked", true);
                break;
                case ui_node.message_off:
                    console.log( $("#switch_"+ui_node.id));
                    $("#switch_"+ui_node.id).prop("checked", false);
                break;
              }
            break;
      
             //Checkbox
             case "checkbox":
                switch(message.payloadString){
                  case ui_node.message_on:
                    console.log( $("#checkbox_"+ui_node.id));
                    $("#checkbox_"+ui_node.id).prop("checked", true);
                    $("#checkbox_label_"+ui_node.id).css("background-color", "#"+ui_node.color);
                    $("#checkbox_label_"+ui_node.id).css("border-color", "#"+ui_node.color);
                  break;
                  case ui_node.message_off:
                      console.log( $("#checkbox_"+ui_node.id));
                      $("#checkbox_"+ui_node.id).prop("checked", false);
                      $("#checkbox_label_"+ui_node.id).css("background-color", "#fff");
                      $("#checkbox_label_"+ui_node.id).css("border-color", "#c0c0c0");
                  break;
                }
              break;
      
            //gauge
            case "gauge":
              console.log(gauges);
              var message_gauges = gauges.filter(function(gauge) {
                return gauge.config.id == "gauge_"+ui_node.id;
              });
              $.each(message_gauges, function(){
                var gauge = this;
                gauge.refresh(message.payloadString);
              });
            break;
      
            //chart
            case "chart":
              var message_charts = charts.filter(function(chart){
                return chart.id == "chart_"+ui_node.id;
              });
              $.each(message_charts, function(){
                var chart = this;
                var currentTime = new Date();
                chart.config.data.labels.push("");
                
                var dataset = chart.config.data.datasets[0];
                dataset.data.push(message.payloadString);
      
                if(dataset.data.length>=10){
                  dataset.data.shift();
                  chart.config.data.labels.shift();
                }
         
                
                chart.update();
              });
            break;
      
            //slider
            case "slider":
                $("#slider_"+ui_node.id).val(message.payloadString);
            break;
            
      
      
            default:
              console.log("[Error]: Message arrived, type not supported - "+ui_node.type);
            break;
          }
          
        });
      }

    //UI nodes
    //Add new ui node button clicked
    $(document).on("click", "#btn_add_new_ui_element", function(){
        form_ui_node_mode = "new";
        $.each($(".tab_nav_ui_node"), function(){
          $(this).css("display","");
        });
        var new_ui_node_inputs = $(".form_add_ui_element").find("input");
        $.each(new_ui_node_inputs, function(){
          //console.log($(this).attr("type"));
         if($(this).attr("type") != "number" && $(this).attr("name") != "type"){
           $(this).val("");
         }
        
        });
    });

       //Delete button
       $(document).on("click", ".btn_delete_ui_node", function(event){
        event.preventDefault();
        var node_id = $(this).data("node-id");
        Swal.fire({
          title: '<strong>Ui element delete</strong>',
          icon: 'warning',
          html:'Are you sure you want to delete the ui element?',
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText:'<i class="fa fa-trash"></i> yes, delete',
          confirmButtonColor: '#FF0000',
          cancelButtonText:'<i class="fa fa-times"></i>no'
        }).then((result) => {
          if (result.value) {
            removeNode(json_ui.nodes, node_id);
            $("#grid-snap_"+node_id).remove();
          }
        });
      });

    //Form edit ui element open
    $(document).on("click", ".btn_edit_ui_node", function(event){
        event.preventDefault();
        form_ui_node_mode = "edit";
        var node_id = $(this).data("node-id");
        selected_node_id = node_id;
        var ui_node = json_ui.nodes.find(function(ui_node) {
          return ui_node.id == node_id;
        });
        console.log(ui_node);
        //activate tab
        $.each($(".tab_nav_ui_node"), function(){
          if($(this).data("type") == ui_node.type){
            $(this).addClass("active");
            $(this).css("display", "");
          }else{
            $(this).removeClass("active");
            $(this).css("display", "none");
          }
        });
        $.each($("#tab_panes_ui_node").find(".tab-pane"),function(){
          if($(this).attr("id")=="tab_add_"+ui_node.type){
            $(this).addClass("active");
          }else{
            $(this).removeClass("active");
          }
        });
        //populate input values from node data
        
        $.each(Object.keys(ui_node), function(index){
          if(this == "color"){
                $("#tab_add_"+ui_node.type).find("input[name=color]").css('background-color', '#'+ui_node.color);
          }
          switch(ui_node.type){
            case "html":
                CKEDITOR.instances.ckEditor_html5_add.setData(ui_node.html); 
            break;
            default:
                $("#tab_add_"+ui_node.type).find(".form-control[name="+this+"]").val(Object.values(ui_node)[index]);
            break;
          }
          
        });
        
    
      });

     //Form Add ui element or edit value
     $(document).on("submit", ".form_add_ui_element", function(event){
        event.preventDefault();
        var formdata = $(this).serializeArray();
        console.log(formdata);
        console.log(form_ui_node_mode);
        switch (form_ui_node_mode){
          case "new": //New node
            var new_node = {};
            new_node.id = create_UUID();
            new_node.pos_x =0;
            new_node.pos_y =0;
        
        
            $.each(formdata, function(){
              console.log(this);
              if (!isNaN(this.value)) { //check if value is convertible to number
                new_node[this.name] = Number(this.value);
              } else {
                new_node[this.name] = this.value;
              }
              
            });
            console.log(new_node);
            json_ui.nodes.push(new_node);
            ui_add_element(new_node);
            if(!ui_edit_enabled){
              $("#btn_toggle_config").trigger("click"); //Enable ui_config
            }else{
              $("#btn_toggle_config").trigger("click"); //Re-enable ui_config
              $("#btn_toggle_config").trigger("click");
            }
            modal("New UI node added successfully");
            break;
            case "edit":
            console.log("edit node");
            node_id = selected_node_id;
            var path_original; //ui node original path to check if it's changed
             //módosítani a json_ui.nodes tartalmát és elmenteni azt
            $.each(json_ui.nodes, function(index){
              var ui_node = this;
              if(ui_node.id == node_id){
                path_original = ui_node.path; //ui node original path to check if it's changed
                //Check if path changed
                if(typeof(client) !== "undefined" && typeof(path_original) !== "undefined" && path_original != $("#tab_add_"+ui_node.type).find("input[name=path]").val() ){
                  //Subscribe to new path
                  client.subscribe($("#tab_add_"+ui_node.type).find("input[name=path]").val());
                  
    
                }
                $.each(json_ui.nodes[index], function(name, value){
                  switch(ui_node.type){
                    case "html":
                        console.log(json_ui.nodes[index]);
                        json_ui.nodes[index].html = CKEDITOR.instances.ckEditor_html5_add.getData();
                    break;
    
                    default:
                        var formelement = $("#tab_add_"+ui_node.type).find(".form-control[name="+name+"]");
                        console.log(formelement);
                        
                        if(formelement.length>0){ //Only change that key where input is found
                          if (!isNaN(  $("#tab_add_"+ui_node.type).find(".form-control[name="+name+"]").val())) { //check if value is convertible to number
                            json_ui.nodes[index][name] = Number($("#tab_add_"+ui_node.type).find(".form-control[name="+name+"]").val());
                          } else {
                            json_ui.nodes[index][name] = $("#tab_add_"+ui_node.type).find(".form-control[name="+name+"]").val();
                          }
                        }
                    break;
                  }
                 
    
                  //Unsubscribe if no other node subscribed to this path
                  var message_nodes = json_ui.nodes.filter(function(ui_node) {//Are there any nodes in the same path remaining?
                    return ui_node.path == path_original;
                  });
                  if(message_nodes.length == 0 && typeof(client) !== "undefined"){//If no, then unsubscribe from this path
                    if(client.isConnected()){
                      console.log("no other node left, unsubscribe of original path");
                      client.unsubscribe(path_original);
                    }
                  }
                 
    
                });
                //console.log(json_ui.nodes[index]);
                //újrarenderelni az elemet
                $("#grid-snap_"+ui_node.id).remove();
                ui_add_element(json_ui.nodes[index]);
    
                //ha az útvonal változott feliratkozni az új útvonalra
               //Todo leiratkozni, ha más elem nincs az előző útvonalon
              }
              modal("UI node edited successfully");
            });
        
          break;
        }

        save_ui(); // Save the UI 
        $(".btn_modal_close").click();
     });

     //UI Add element function
   function ui_add_element(ui_node){
    var html = `
      <div id="grid-snap_`+ui_node.id+`" data-node-id="`+ui_node.id+`" class="ui_div" style="position:absolute;">`;
  
      switch(ui_node.type){
        case "button":
            html += `<button class="btn ui_button btn-block btn-primary" type="button" style="width:200px; " data-node-id="`+ui_node.id+`">`+ui_node.text+`</button>`
        break;
  
        case "label":
            html +=`<div style="color:#`+ui_node.color+`">`+ ui_node.text +`</div>`+`<br>`;
        break;
  
        case "html":
          html += ui_node.html +`<br>`;
         break;
  
        case "switch":
            html += `<label class="switch switch-label switch-pill switch-primary">
           <input id="switch_`+ui_node.id+`"" class="ui_switch switch-input" type="checkbox" data-node-id="`+ui_node.id+`">
           <span class="switch-slider" data-checked="On" data-unchecked="Off"></span>
          </label>
          <br>`
        break;
  
        case "checkbox":
            html += `<div class="round">
            <input id="checkbox_`+ui_node.id+`"" class="ui_checkbox" type="checkbox" data-node-id="`+ui_node.id+`">
            <label id="checkbox_label_`+ui_node.id+`"" for="checkbox_`+ui_node.id+`""></label>
            </div>
           <br>`
        break;
  
        case "input_text":
            html += `<div style="width:200px;">
           
              <input  data-node-id="`+ui_node.id+`" class="form-control input_text" type="text" placeholder="Input text and press enter">
            
            </div>`
  
        break;
  
        case "slider":
            html += `
              <input id="slider_`+ui_node.id+`" type="range" orient="${ui_node.orientation}" class="slider ui_slider" min="`+ui_node.value_min+`" max="`+ui_node.value_max+`" data-node-id="`+ui_node.id+`"></input>
            <br>`;
        break;
  
        case "colorpicker":
            html += `
            <input id="colorpicker_`+ui_node.id+`" class="jscolor ui_colorpicker" data-node-id="`+ui_node.id+`">
            <br>`;
            
        break;
  
        case "gauge":
            html += `<div id="gauge_`+ui_node.id+`" class="200x160px"></div>
            <br>`;
        break;
  
        case "chart":
            html += `<canvas id="chart_`+ui_node.id+`" style="display: block; width: 500px; height: 300px;" width="500" height="300" class="chartjs-render-monitor"></canvas>
            <br>`;
        break;
  
        default:
          console.log("[ERROR] ui_node.type not found: " + ui_node.type);
        break;
      }
    
    
    html += `<div class="ui_div_helper btn-group float-right">
              <button class="btn btn-transparent btn_edit_ui_node" data-node-id="`+ui_node.id+`" data-toggle="modal" data-target="#modal_add_new_ui_node"aria-haspopup="true" aria-expanded="false">
                <i class="icon-settings"></i>
              </button>
              <button class="btn btn-transparent btn_delete_ui_node" data-node-id="`+ui_node.id+`" >
                <i class="icon-trash" style="color:red;"></i>
              </button>
  
            </div>
          </div>
        </div>`;
        
    
    $("#client_ui").append(html);
   
  
    //activate jscolor
    if(ui_node.type=="colorpicker"){
      var input = document.getElementById("colorpicker_"+ui_node.id);
      var picker = new jscolor(input);
      picker.id= "colorpicker_"+ui_node.id;
      colorpickers.push(picker);
      //var picker = new jscolor($.("#colorpicker_"+ui_node.id));
    }
  
    //activate gauge
    if(ui_node.type=="gauge"){
      var g = new JustGage({
        id: "gauge_"+ui_node.id,
        value: ui_node.value_max/2,
        min: ui_node.value_min,
        max: ui_node.value_max,
        title: ui_node.text
      });
      gauges.push(g);
    };
  
    //activate chart
  
    if(ui_node.type=="chart"){
      var config = {
              type: 'line',
              
              options: {
          
                  responsive: true,
                  title: {
                      display: true,
                      text: ui_node.text
                  },
                  tooltips: {
                      mode: 'index',
                      intersect: false,
                  },
                  hover: {
                      mode: 'nearest',
                      intersect: true
                  },
                  scales: {
                      xAxes: [{
                          display: true,
                          scaleLabel: {
                              display: true,
                              labelString: 'Time'
                          }
                      }],
                      yAxes: [{
              type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
              display: true,
              position: "left",
              id: "y-axis-1",
              ticks: {
                min: ui_node.value_min,
                max: ui_node.value_max
            }
          }],
                  }
              }
      };
      
      var ctx = document.getElementById('chart_'+ui_node.id).getContext('2d');
      //console.log(ctx);
      ctx = new Chart(ctx, config);
      ctx.id = "chart_"+ui_node.id;
  
      charts.push(ctx);
      
      //Add dateset test
      var Dataset = {
        label: ui_node.text,
        data: [],
        fill: false,
        backgroundColor: "black"
      };
  
      config.data.datasets.push(Dataset);
  
      
     ctx.update();
    }
  
      //Subscribe to path if client is connected
      if(ui_node.path != null && typeof(MQTTclient) !== "undefined"){
        if(MQTTclient.isConnected()){
            MQTTclient.subscribe(ui_node.path);
        }
     }
    
  
    var element = document.getElementById('grid-snap_'+ui_node.id);
  
  var x = ui_node.pos_x; var y =ui_node.pos_y;
  
  element.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
  
  interact(element)
    .draggable({
      modifiers: [
        interact.modifiers.snap({
          targets: [
            interact.createSnapGrid({ x: 10, y:10 }) //ugrások száma
          ],
          range: Infinity,
          relativePoints: [ { x: 0, y: 0 } ]
        }),
        interact.modifiers.restrict({
          restriction: element.parentNode,
          elementRect: { top: 0, left: 0, bottom: 0, right: 0},
          endOnly: true
        })
      ],
      inertia: true
    })
    .on('dragmove', function (event) {
      x += event.dx
      y += event.dy
      console.log(event.target);
      event.target.style.webkitTransform =
      event.target.style.transform =
          'translate(' + x + 'px, ' + y + 'px)'
          console.log(x);
          console.log(y);
      //save new position
      var node_id = $(event.target).data('node-id');
      console.log(node_id);
      $.each(json_ui.nodes, function(){
        if(this.id == node_id){
          console.log(this);
          this.pos_x = x;
          this.pos_y = y;
        }
      });
      save_ui();
          
    });
        
  
  
  }

  //remove element from array
  function removeNode(array, node_id) {
    $.each(array, function(){
      var ui_node = this;
      
      if(ui_node.id == node_id){
  
        var deleted_node_path = this.path;
       
        
  
  
        var index = array.indexOf(ui_node);
        if (index > -1) {
            array.splice(index, 1);
        }
  
        var message_nodes = array.filter(function(ui_node) {//Are there any nodes in the same path remaining?
          return ui_node.path == deleted_node_path;
        });
        if(message_nodes.length == 0 && this.path != null && typeof(client) !== "undefined"){//If no, then unsubscribe from this path
          if(client.isConnected()){
            client.unsubscribe(ui_node.path);
          }
        }
        
  
      }
    });
    save_ui();
    modal("UI node removed successfully.");
  }

  $(document).on("mouseover", ".ui_div", function(){
    if(ui_edit_enabled){
      var topZ = 0;
      $('.ui_div').each(function(){
        var thisZ = parseInt($(this).css('z-index'), 10);
        $(this).css("opacity", ".5");
        if (thisZ > topZ){
          topZ = thisZ;
        }
      });
      $(this).css('z-index', topZ+1);
      $(this).css("opacity", ".9");
    } 
  });
  
  
  $(document).on("mouseleave", ".ui_div", function(){
    $('.ui_div').each(function(){
      $(this).css("opacity", "1");
    });
  });
  
  $(document).on("click", "#btn_toggle_config", function(event){
    event.preventDefault();
    if(!ui_edit_enabled){
      $.each($(".ui_div"), function(){
        interact(this).draggable(true);
        $(this).css("background-color", "#c0c0c0");
      });
      $(".ui_div_helper").css("display", "");

      $("#client_ui").css("background-color", "#00bbff");
      $("#client_ui").css("background-image", "linear-gradient(white 1px, transparent 1px), linear-gradient(90deg, white 2px, transparent 2px), linear-gradient(rgba(255,255,255,.3) 1px, transparent 1px), linear-gradient(90deg, rgba(255,255,255,.3) 1px, transparent 1px)");
       $("#client_ui").css("background-size", "100px 100px, 100px 100px, 20px 20px, 20px 20px");
       $("#client_ui").css("background-position", "-2px -2px, -2px -2px, -1px -1px, -1px -1px");


      ui_edit_enabled = true;
      modal("UI nodes configuration enabled");
    }else{
      $.each($(".ui_div"), function(){
        interact(this).draggable(false);
        $(this).css("background-color", "");
      
      });
      $(".ui_div_helper").css("display", "none");

      $("#client_ui").css("background-color", "white");
      $("#client_ui").css("background-image", "");
       $("#client_ui").css("background-size", "");
       $("#client_ui").css("background-position", "");
      ui_edit_enabled = false;
      modal("UI nodes configuration disabled");
    }
      
      
      //$(".ui_div").css("background", "transparent");
      //$(".ui_div_helper").css("display", "none")
  });

   //Button
   $(document).on("click", ".ui_button", function(){
    var node_id = $(this).data("node-id");
    var message_nodes = json_ui.nodes.filter(function(ui_node) {
      return ui_node.id == node_id;
    });
    //console.log(message_nodes);
    $.each(message_nodes, function(){

      var ui_node = this;

      if(ui_node.type=="button"){
        var message = new Paho.Message(ui_node.message);
        message.destinationName = ui_node.path;
        MQTTclient.send(message);
      }
    });
    modal("Button: Message sent");
  });

  //Input
  $(document).on("keypress", ".input_text", function(e){
    if (e.which == 13) {
      var node_id = $(this).data("node-id");
      var input_message = $(this).val();
      var message_nodes = json_ui.nodes.filter(function(ui_node) {
        return ui_node.id == node_id;
      });
      //console.log(message_nodes);
      $.each(message_nodes, function(){
  
        var ui_node = this;
  
        if(ui_node.type=="input_text"){
          var message = new Paho.Message(input_message);
          message.destinationName = ui_node.path;
          MQTTclient.send(message);
        }
      });
      modal("Input Text: Message sent");
    }  
   
  });

  //Switch
  $(document).on("change", ".ui_switch", function(){
    var checkbox = $(this);
    var node_id = $(this).data("node-id");
    var message_nodes = json_ui.nodes.filter(function(ui_node) {
      return ui_node.id == node_id;
    });
    //console.log(message_nodes);
    $.each(message_nodes, function(){
  
      var ui_node = this;
  
      if(ui_node.type=="switch"){
        if($(checkbox).is(":checked")){
          var message = new Paho.Message(ui_node.message_on);
          message.destinationName = ui_node.path;
          MQTTclient.send(message);
        }else{
          var message = new Paho.Message(ui_node.message_off);
          message.destinationName = ui_node.path;
          MQTTclient.send(message);
        }
      }
    });    
    modal("Switch: Message sent");
  });

  //Checkbox
  $(document).on("change", ".ui_checkbox", function(){
    var checkbox = $(this);
    var node_id = $(this).data("node-id");
    var message_nodes = json_ui.nodes.filter(function(ui_node) {
      return ui_node.id == node_id;
    });
    //console.log(message_nodes);
    $.each(message_nodes, function(){
  
      var ui_node = this;
  
      if(ui_node.type=="checkbox"){
        if($(checkbox).is(":checked")){
          $("#checkbox_label_"+ui_node.id).css("background-color", "#"+ui_node.color);
          $("#checkbox_label_"+ui_node.id).css("border-color", "#"+ui_node.color);
          var message = new Paho.Message(ui_node.message_on);
          message.destinationName = ui_node.path;
          MQTTclient.send(message);
        }else{
          $("#checkbox_label_"+ui_node.id).css("background-color", "#fff");
          $("#checkbox_label_"+ui_node.id).css("border-color", "#c0c0c0");
          var message = new Paho.Message(ui_node.message_off);
          message.destinationName = ui_node.path;
          MQTTclient.send(message);
        }
      }
    });    
    modal("Switch: Message sent");
  });

  //colorpicker
  $(document).on("change", ".ui_colorpicker", function(){
    var colorpicker = $(this);
    var node_id = $(this).data("node-id");
    var message_nodes = json_ui.nodes.filter(function(ui_node) {
      return ui_node.id == node_id;
    });
    $.each(message_nodes, function(){
      var ui_node = this;
      var message =  new Paho.Message("#"+$(colorpicker).val());
      message.destinationName = ui_node.path;
      MQTTclient.send(message);
    });
    modal("Color Picker: Message sent");
  });

  //slider
  $(document).on("change", ".ui_slider", function(){
    var slider = $(this);
    var node_id = $(this).data("node-id");
    var message_nodes = json_ui.nodes.filter(function(ui_node) {
      return ui_node.id == node_id;
    });
    $.each(message_nodes, function(){
      var ui_node = this;
      var message =  new Paho.Message($(slider).val());
      message.destinationName = ui_node.path;
      MQTTclient.send(message);
    });
    modal("Slider: Message sent");
  });
     

});